﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace ex1
{
    public partial class Form2 : Form
    {
        String USER;
        public Form2(String user)
        {
            InitializeComponent();
            USER = user;
        }

        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
        {
            String checkDate = monthCalendar1.SelectionRange.Start.ToShortDateString();
            if (checkDate[0] == '0' || checkDate[3] == '0')
            {
                String[] spl = checkDate.Split('/');
                if (checkDate[0] == '0')
                {
                    spl[0] = spl[0].Replace("0", "");
                }
                if (checkDate[3] == '0')
                {
                    spl[1] = spl[1].Replace("0", "");
                }
                checkDate = spl[1] + "/" + spl[0];
            }
            String fileName = USER + "BD.txt";

            if (!File.Exists(fileName))
            {
                string msg = "Error";
                MessageBoxButtons btns = MessageBoxButtons.OK;
                MessageBox.Show("No file, loaded\nCreating new file", msg, btns);
                File.Create(fileName);
                TextWriter write = new StreamWriter(fileName);
                write.WriteLine("Shiri,8/28/2001");
                write.Close();
            }
            System.IO.StreamReader file = new System.IO.StreamReader(fileName);
            String lines = file.ReadLine();
            String[] spltd = lines.Split(',');
            while (true)
            {
                String[] splt = spltd[1].Split('/');
                spltd[1] = splt[0] + '/' + splt[1];
                if (spltd[1] == checkDate)
                {
                    label2.Text = "On the selected date: " + spltd[0] + " has a birthday";
                    break;
                }
                if (!file.EndOfStream)
                {
                    lines = file.ReadLine();
                    spltd = lines.Split(',');
                }


            }
            if (file.EndOfStream && (label2.Text == "On the selected date nobody has a birthday" || label2.Text == ""))
            {
                label2.Text = "On the selected date nobody has a birthday";

            }
        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}
